#ifndef Z0_B64_H
#define Z0_B64_H

#include <stdint.h>
#include <stdbool.h>

typedef uint32_t u32;
typedef uint8_t  u8;

typedef struct {
	bool  valid; // the size without this field is 16byte, 
		     // so we can use the extra memory here.
	u32   len;
	u8   *data;
} z0_b64;

void
z0_b64_encode(z0_b64*, u8*, u32);

void
z0_b64_decode(z0_b64*, u8*, u32*);

void
z0_b64_free(z0_b64*);

void
z0_b64_valid(z0_b64*);

void
z0_b64_print(z0_b64*);

#endif
