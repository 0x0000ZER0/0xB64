# 0xB64

A `base64` implementation in `C`.

### API

- Encoding a string (note that at this step the **valid** member checks only if the malloc function returned *NULL* or not). Also note that the **z0_b64** struct does not waste an extra space by including the boolean **valid** member:

```c
char txt[] = "saudysa";

z0_b64 obj;
z0_b64_encode(&obj, txt, sizeof (txt) - 1);

// check if the output is valid:
if (obj.valid) {
        //...
        obj.len;   // the length of the data member.
        obj.data;  // the encoded data member.
}


// don't forget to free the data.
z0_b64_free(&obj);
```

- Decoding the given string. The function will assume that the caller allocated memory:

```c
z0_b64 obj = {
    .data  = "OGQ2c2Fic2Q5OGFjbiAxMjE=",
    .len   = 25,
    .valid = true
};

u32  len;
u8   seq[25];
ptr = z0_b64_decode(&obj, &len);

//...

```

- Validating the `z0_b64` object. This method will actually check if you object does contain valid data or not:

```c
z0_b64 obj = {
    .data  = "OGQ2c2Fic2Q5OGFjbiAxMjE=",
    .len   = 25
};
z0_b64_valid(&obj);

if (obj.valid) {
        //...
}
```

- Printing the object:

```c
z0_b64 obj;
z0_b64_print(&obj);

// will print:
// Z0 B64 LEN:   {obj.len}
// Z0 B64 DATA:  {obj.data}
// Z0 B64 VALID: {obj.valid}
```


